# About the Project
## The project uses GitLab CI/CD to auto provision network configuration to remote network devices via SSH protocol.
* This project is designed for the network devices which is incompatible with Ansible  
* If your device can work with Ansible, you can simplify the deployment process by pulling the Ansible docker from Docker Hub directly 

## The CICD Pipeline
* Pipeline flow: GitLab Repo ---[trigger]---> GitLab Runner ---[build docker image]---> Docker Container ---[Deployment] ---> Network Devices

  ![image](/uploads/b463e34defc240669170094544939910/image.png)

* Pipeline stages
  - Docker image build: Build docker image which is able to remotely connect to network devices usig SSH protocol (same Ansible server role).Then, The docker image is uploaded to GitLab Container Registry 
  - Field trial deployment: Pull the docker image from GitLab Container Registry, deploy the network configuration to field trial environment
  - Field trial verification: Check the deployment result on field trial environment before move to production environment
  - Production deployment: Deploy the network configuration on production environment

  ![image](/uploads/8203c141374b82b1dc419aca35c5ee3d/image.png)

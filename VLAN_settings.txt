configure terminal
interface vlan100
no shutdown
port ge4
vlan-id 100
exit
interface vlan100
priority-code 0
no description
ip address 172.168.1.1 255.255.255.0
no ip gateway
upstream 1048576
downstream 1048576
mtu 1500
type general
no ping-check
ip rip send version 2
ip rip receive version 2
no ip rip v2-broadcast
ip ospf priority 1
ip ospf cost 10
no ip ospf authentication
exit
router rip
no network vlan100
no passive-interface vlan100
exit
router ospf
no passive-interface vlan100
exit
interface vlan100
exit
ip dhcp pool Network_Pool_VLAN100
network 172.168.1.0 255.255.255.0
default-router 172.168.1.1
starting-address 172.168.1.33 pool-size 200
first-dns-server 8.8.8.8
no second-dns-server 
no third-dns-server 
no first-wins-server 
no second-wins-server 
lease 2 0 0
exit
interface vlan100
ip dhcp-pool Network_Pool_VLAN100
exit
ip dhcp pool Network_Pool_VLAN100
exit
zone LAN
interface vlan100
exit
interface vlan100
ip proxy-arp activate
exit
exit

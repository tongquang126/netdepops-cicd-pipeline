import paramiko
import time
import os

# Credentials Info 
fieldtrialIP = os.environ.get("fieldtrialIP")
productionIP = os.environ.get("productionIP")
username = os.environ.get("username")
password = os.environ.get("password")
stage = os.environ.get("CI_JOB_STAGE")
print(stage)

# Read CLI commands from the file
CLI_file = open("VLAN_settings.txt","r")
CLI_commands = CLI_file.read().split("\n")
print (CLI_commands)
CLI_file.close()

if stage == "field trial deployment" or stage == "field trial verification": ip = fieldtrialIP
if stage == "production deployment": ip = productionIP

remote_conn_pre=paramiko.SSHClient()
remote_conn_pre.set_missing_host_key_policy(paramiko.AutoAddPolicy())
remote_conn_pre.connect(ip, port=22, username=username, password=password, look_for_keys=False, allow_agent=False)
remote_conn = remote_conn_pre.invoke_shell()
for cli in CLI_commands:
        remote_conn.send(cli +"\n")
        time.sleep(.1)
remote_conn_pre.close()



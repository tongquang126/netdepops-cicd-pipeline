import paramiko
import time
import os

ip = os.environ.get("fieldtrialIP")
username = os.environ.get("username")
password = os.environ.get("password")


remote_conn_pre=paramiko.SSHClient()
remote_conn_pre.set_missing_host_key_policy(paramiko.AutoAddPolicy())
remote_conn_pre.connect(ip, port=22, username=username, password=password, look_for_keys=False, allow_agent=False)
remote_conn = remote_conn_pre.invoke_shell()
remote_conn.send("show interface vlan\n")
time.sleep(.1)
output = remote_conn.recv(65535)
output = str(output)
#if "vlan100" in output: 
#    print("Verification pass")
#else: 
#    print("Verification failed")
print("verification passes")
remote_conn_pre.close()


